$(document).keydown(function(event){    
    var key = event.which;                
        switch(key) {
            case 38:
            goToPrevAnchor();
                break;
            case 40:
            goToNextAnchor();
                break;
        }   
    });

    window.addEventListener("keydown", function(e) {
        if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
            e.preventDefault();
        }
    }, false);

    function scrollFunction(event){
        var y = event.deltaY;
        if (y > 0) {
            goToNextAnchor();
        } else{
            goToPrevAnchor();
        }
    }

    function goToNextAnchor() {
        var anchors = document.anchors;
        var loc = window.location.href.replace(/#.*/,'');
        var nextAnchorName;
        var anchorName = window.location.hash.replace(/#/,'');
        if (!anchorName) {
            nextAnchorName = anchors[0].name;
        }
        if (anchorName) {
            for (var i=0, iLen=anchors.length; i<iLen-1; i++) {
                if (anchors[i].name == anchorName) {
                    nextAnchorName = anchors[++i].name;
                    break;
                }
            }
        }
        if (!nextAnchorName) {
            nextAnchorName = anchorName;
        }
        window.location.href = loc + '#' + nextAnchorName;
    }

    function goToPrevAnchor() {
        var anchors = document.anchors;
        var loc = window.location.href.replace(/#.*/,'');
        var prevAnchorName;
        var anchorName = window.location.hash.replace(/#/,'');
        if (anchorName) {
            for (var i=1, iLen=anchors.length; i<iLen; i++) {
                if (anchors[i].name == anchorName) {
                    prevAnchorName = anchors[--i].name;
                    break;
                }
            }
        }
        if (!prevAnchorName) {
            prevAnchorName = anchorName;
        }
        window.location.href = loc + '#' + prevAnchorName;
    }